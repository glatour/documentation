# documentation

This is a collection of interesting resources & explanations that I've stumbled upon or produced during my internship at INRIA LACODAM.

## How to summerize

### The Rhetorical Précis Format


- In a single coherent sentence give the following:
    - name of the author, title of the work, date in parenthesis;
    - a rhetorically accurate verb (such as "assert," "argue," "deny," "refute," "prove," disprove," "explain," etc.);
    - a that clause containing the major claim (thesis statement) of the work.
- In a single coherent sentence give an explanation of how the author develops and supports the major claim (thesis statement).
- In a single coherent sentence give a statement of the author's purpose, followed by an "in order" phrase.
- In a single coherent sentence give a description of the intended audience and/or the relationship the author establishes with the audience.

> Charles S. Peirce's article, "The Fixation of Belief" (1877), asserts that humans have psychological and social mechanisms designed to protect and cement (or "fix") our beliefs.
> 
> Peirce backs this claim up with descriptions of four methods of fixing belief, pointing out the effectiveness and potential weaknesses of each method.
> 
> Peirce's purpose is to point out the ways that people commonly establish their belief systemsin order tojolt the awareness of the reader into considering how their own belief system may the product of such methods and to consider what Peirce calls "the method of science" as a progressive alternative to the other three.
> 
> Given the technical language used in the article, Peirce is writing to an well-educated audience with some knowledge of philosophy and history and a willingness to other ways of thinking.