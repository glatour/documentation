# Read articles summary

- [Read articles summary](#read-articles-summary)
  - [Explaining Embedding-based Models for Link Prediction in Knowledge Graphs](#explaining-embedding-based-models-for-link-prediction-in-knowledge-graphs)
  - [Extracting Interpretable Models from Matrix Factorization Models](#extracting-interpretable-models-from-matrix-factorization-models)

## Explaining Embedding-based Models for Link Prediction in Knowledge Graphs

> Luis Galárraga, “Explaining Embedding-based Models for Link Prediction in Knowledge Graphs,” Feb. 2021.

This article proposes a framework to explain black-box latent space link prediction models, using KG mined rules as features of a logistic regression model.

## Extracting Interpretable Models from Matrix Factorization Models

> I. S. Carmona and S. Riedel, “Extracting Interpretable Models from Matrix Factorization Models,” p. 7.

This experience consist on analyzing fidelity and interpretability of MF (Matrix Factorization) models with post-hoc models such as Bayes Network, Decision tree and Logic Rule.
