# Review summary

- [Review summary](#review-summary)
  - [Major](#major)
    - [Lack of evaluation, lack of comparison](#lack-of-evaluation-lack-of-comparison)
      - [How good are the chosen metric to measure fidelity ?](#how-good-are-the-chosen-metric-to-measure-fidelity-)
      - [State of the art approaches](#state-of-the-art-approaches)
    - [How well do the mined rules serve as explanations ?](#how-well-do-the-mined-rules-serve-as-explanations-)
      - [What is the definition of explanation ?](#what-is-the-definition-of-explanation-)
    - [How the initial context C is chosen in practice ?](#how-the-initial-context-c-is-chosen-in-practice-)
    - [Improve writing accuracy](#improve-writing-accuracy)
  - [Minor](#minor)
    - [Questions](#questions)
    - [Typo and quick fix](#typo-and-quick-fix)

## Major

### Lack of evaluation, lack of comparison

> My main issue with the paper is the lack of evaluation.
> The experiment of 5.2 evaluates how well the mined rules fit the data.
> It is illustrative but it doesn't function as an evaluation, because the system is not compared against anything.
> 
> I can look up how good these numbers are compared to other link predictors, but that isn't too relevant, because the system is not presented as a link predictor.
>
> AnonReviewer2 - p. 2

#### How good are the chosen metric to measure fidelity ?
> It is hard to understand why classification and ranking metrics on a certain knowledge graph can be treated as the fidelity degree for mimicking an embedding model.
> 
> AnonReviewer1 - p. 3

#### State of the art approaches
> Combined existing approaches leveraging symbolic and embeddings based predictions are not covered in related work.
> 
> Summary - p. 1

> The paper divides approaches into symbolic and embedding-based; however, there are several approaches that combine both (ref. in review). 
> The author mentions them in the related work; however, what is missing is a discussion of what these approaches miss to be successful.
> 
> AnonReviewer5 - p. 4

> Second, a stronger motivation could be provided to demonstrate the novelty of the proposed approach; 
> Moreover, while it is good to conduct some ablation experiments, it would be necessary to compare the proposed method with state-of-the-art baselines.
> 
> AnonReviewer3 - p. 5

> The proposed method is actually to learn a set of rules to perform the task of link prediction.
> The experiments represented in the paper are basically a kind of ablation experiments.
> A comparative study could be conducted, while such experiments have been reported in the literature.
> 
> AnonReviewer3 - p. 5

> The author correctly refers to existing solutions for providing explanations to link predictions, however no comparative experimental study is provided. At least a discussion at this regards should have been reported.
> 
> AnonReviewer4 - p. 7

### How well do the mined rules serve as explanations ?

> The fundamental question : how well do these rules serve are explanations (compared to other rules) does not seem to be addressed.
> 
> Summary - p. 1


What did the author of this comment mean ?
- "Are the mined rules sufficient for an human to understand anything ?"
- "do the provided rules explain (better than the other rules) the decisions made by the link predictor ?"
- something else ?

In both case it seems that a comparison between other rule language is mandatory.
Here is some of them :
- Horn
- Association
- Existential
- Mix Horn-association

I think that the first interpretation ("Are the mined rules sufficient for an human to understand anything ?") is really interesting but is too wide for us to answer.

It implies multiple survey designed to measure the interpretability (understandability?) of some rules and I don't think that it is pertinent for us to go in that direction.

The other interpretation ("do the provided rules explain (better than the other rules) the decisions made by the link predictor ?") wants to compare different rule languages and provide a hierarchy among them based on the explainability.

This implies some kind of metric that would efficiently measure explainability (and interpretability ?), and an experiment that would run all the rule mining on the same link predictor to show which rule language is best.

Finally this question should be read with the light of the first comment of the section [lack of comparison](#lack-of-evaluation-lack-of-comparison)

> A systematic evaluation, i.e. in what sense the mined rules truly explain the predictions, is missing.
> Rather, it is criticized by the reviewer that the computed explanations are naturally dependent on both the given embedding model and the given knowledge graph.
> 
> Summary - p. 1

#### What is the definition of explanation ?

> The definition of "explanation" deserves more attention. 
> While this approach fits some existing methods billed as explainable AI, it doesn't provide a causal explanation of what the model does. 
> The learned rule may be that a person is predicted to speak a particular language because their parent does, but that is no guarantee that that is why the embedding method makes this prediction. 
> Compare this, for instance, with a method that aims to assign semantics to the learned dimensions. 
> That would be more causal than the current approach. 
> This matter should be discussed up front, with a clear statement of what constitutes an explanation in the context of this paper.
> 
> AnonReviewer2 - p. 2

The reviewer value the causality of an explanation and stress its importance.

The difference between interpretability, explainability and maybe other human-related terms should be defined so everyone can align with the research framework

> The definition of explanation seems arbitrary. 
> It is not clear why this is the right definition. 
> Moreover, there is no discussion at all about other potential definitions of explanations.
> 
> AnonReviewer5 - p. 4

### How the initial context C is chosen in practice ?

> It is unclear to me how the initial context C is chosen in practice. Section 4.2 "proposes" (does that mean this is done in practice?) a method of designing a local context for each target triple, but these triples do exist in K, when section 4.1 states that they shouldn't.
>
> AnonReviewer2 - p. 2

> The locality issue, that is briefly mentioned by the end of section 1, should be treated in a more explicit way otherwise it does not result clear at this point.
> 
> AnonReviewer4 - p. 6

> I would suggest to first introduce the notion of context (which suddenly appears here) and clarify its role, hens proceeding with the description of the algorithm.
> 
> AnonReviewer4 - p. 6

### Improve writing accuracy

> Only a vague intuition of the process for learning explanations is given, whilst this part should represent almost the main core of the contribution
>
> AnonReviewer4 - p. 6


> First, the presentation could be significantly improved by precisely formulating major definitions and algorithms.
> 
> AnonReviewer3 - p. 5

This could be part of the minor issues if the reviewer hadn't use the word "significantly".

> The (abstract) definition of link prediction does not characterize the task of link prediction,
> for instance, how can one distinguish the unknown facts from facts to be added ?
> It looked the definition is still at too high level.
> 
> AnonReviewer3 - p. 5

Since I don't know the answer to this question, I put the remark in the "major" section.

## Minor

### Questions

- Amie is an interpretable link predictor. Is there really a need to put a black box in the middle if it only hurts performance ?
- $D_t(\mathcal{K})$ and $D_p(\mathcal{K})$ denote the sets of IRIs and predicates present in the KG. Can you please clarify the distinction between the predicated and IRIs ? Do predicates refer to logical rules ? [p. 4]
- explain the following sentence [p. 6]: 
    > In that spirit, a fact $A = p(s; o) \in
\Omega(\mathcal{K})$ could be mapped to a $k'$-dimensional vector $x_A$ such that its $j$-th component $x_A[j]$ is greater than zero, if the rule's conclusion is aligned with $\hat{f}$'s answer.
- (Section 4.2) In which case the algorithm the conversion function assigns a score of 0 to the entry ? [P. 7]
- (section 5.1) How many times is called Algorithm 1 ? [p. 7]
- Is the following sentence (p.2) correct ? [p. 7]
    > On the other hand, approaches based on embeddings [4, 14, 18, 19, 25, 27, 28, 30] model predicates and entities in a KG as mathematical structures in a latent space.

### Typo and quick fix

- do not use red on blue in figure 1
- do not abbreviate "figure"
- ~~"title suppressed due to excessive length"~~
- ~~reference to nonexisting lines from algorithm~~
- make distinction between the original (incomplete) KG and the "unknown complete graph" because $\mathcal{K}$ is used for both. [p. 2]
- $\mathcal{K}^-$ and $\mathcal{K}^+$ are not clearly defined [p. 5]
- "KG" and "KB" are used in section and algorithm; it should be precised that those are the same [p. 5]
- write down that we use CWA to generate counter examples [p. 5]
- in section 4.1, sub "Binarizing the black box", recall the meaning and role of function h. [p. 6]
- provide details (process and goal) for rule mining [p. 6]
- explicitly state that only rules having $p_{\hat{f}}(s;o)$ or $\neg p_{\hat{f}}(s;o)$ are mined. [p. 7]
- provide more details about the application of the logistic regression as the final step. [p. 7]
- provide details on how training and testing set have been obtained (+ configuration) [p. 7]
- (section 5.1) provide explanation of the results in the tables. [p. 7]
- after saying "we tuned the number of latent factors for each model and dataset according to the experimental setups proposed in [18,23]", provide the used configuration. [p. 7]
- provide the link to the source code & experimental data in section 5 instead of the conclusion. [p. 7]