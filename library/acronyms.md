# Acronyms

| Acronym  | Definition                                                   |
| -------- | ------------------------------------------------------------ |
| AI       | Artificial Intelligence                                      |
| ALE      | Accumulated Local Effect                                     |
| ALS      | Alternating Least Squares                                    |
| AUC      | Area Under the Curve                                         |
| BFO      | Basic Formal Ontology                                        |
| BN       | Bayesian Network                                             |
| CalL     | Calibration Loss                                             |
| CalB     | Calibration by Bins                                          |
| CPD      | Conditional Probability Distribution (in BN)                 |
| CRM      | Client Relationship Management                               |
| CWA      | Closed World Assumption                                      |
| DAWG     | Data Access Working Group                                    |
| DT       | Decision Tree                                                |
| EA       | Entity Alignment                                             |
| EMF      | Explainable Matrix FActorization                             |
| ER       | Entity Resolution                                            |
| HIN      | Heterogeneous Information Network                            |
| ICE      | Individual Conditional Expectation                           |
| ILP      | Inductive Logic Programming                                  |
| IRI      | International Resource Identifier                            |
| KB       | Knowledge Base                                               |
| KG       | Knowledge Graph                                              |
| KGC      | Knowledge Graph Completion                                   |
| KGE      | Knowledge Graph Embedding                                    |
| KGRL     | Knowledge Graph Representation Learning                      |
| KNN      | k-Nearest Neighbors                                          |
| KR²      | Knowledge Representation and Reasoning                       |
| KR&R     | Knowledge Representation and Reasoning                       |
| LIME     | Local Interpretable Model-agnostic Explanations              |
| LGG      | Least General Generalization                                 |
| LOD      | Linked Open Data                                             |
| LVM      | Latent Variable Model                                        |
| MAE      | Mean Absolute Error                                          |
| MAPR     | Macro Average mean Probability Rate                          |
| MAvA     | Macro Average Arithmetic                                     |
| MAvG     | Macro Average Geometric                                      |
| MF       | Matrix Factorization (models)                                |
| MLP      | Multi-Layer Perceptrons                                      |
| MPR      | Mean Probability Rate                                        |
| MRR      | Mean Reciprocal Rank                                         |
| MSE      | Mean Squared Error                                           |
| NLP      | Natural Language Processing                                  |
| NNMF     | Non-Negative Matrix Factorization                            |
| NTN      | Neural Tensor Network                                        |
| OWA      | Open World Assumption                                        |
| OWL      | Web Ontology Language                                        |
| PCA      | Principal Component Analysis                                 |
| PCA      | Partial Completeness Assumption                              |
| PDP      | Partial Dependence Plot                                      |
| PRA      | Path Ranking Algorithm                                       |
| PSL      | Probabilistic Soft Logic                                     |
| RBMs     | Restricted Boltzmann Machines                                |
| ROC      | Receiver Operator Characteristic                             |
| RDF      | Resource Description Framework                               |
| RDFS     | RDF ?                                                        |
| SFE      | Subgraph Feature Extraction                                  |
| SLE      | Sparse Linear Explanations                                   |
| SKOS     | Simple Knowledge Organization System                         |
| SP       | Sub-modular Pick                                             |
| SPARQL   | SPARQL Protocol and RDF Query Language                       |
| SSE      | Sum of Squares Error                                         |
| SST      | Sum of Squares Total                                         |
| SVD      | Singular Value Decomposition                                 |
| SVM      | Support Vector Machine                                       |
| URI      | Uniform Resource Identifier                                  |
| XKE-PRED | Explaining Knowledge Embedding Model with Predicted features |



## Campus Rennes

| Acronyme | Définition                                                    |
| -------- | ------------------------------------------------------------- |
| CNRS     | Centre National de Recherche Scientifique                     |
| CROUS    | Centre Régional des Oeuvres Universitaires et Scolaires       |
| DSE      | Dossier Social Etudiant                                       |
| ENSCR    | Ecole Nationale Supérieure de Chimie de Rennes                |
| ESIR     | École Supérieure d'Ingénieurs de Rennes                       |
| INRIA    | Institut National de Recherche en Informatique et Automatique |
| INSA     | Institut National des Sciences Appliquées (de Rennes)         |
| IRISA    | Institut de Recherche en Informatique et Systèmes Aléatoires  |
| ISTIC    | ?                                                             |
| IUT      | Institut Universitaire de Technologie (de Rennes)             |
| LACODAM  | LArge scale COllaborative DAta Mining                         |
| UFR      | Unité de Formation et de Recherche                            |