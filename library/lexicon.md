# Lexicon

- [Lexicon](#lexicon)
  - [Knowledge base](#knowledge-base)
  - [Knowledge graph](#knowledge-graph)
  - [Knowledge Graph Completion](#knowledge-graph-completion)
    - [KGC Techniques](#kgc-techniques)
  - [Linked (open) data](#linked-open-data)
  - [Ontology](#ontology)
  - [Tacit Knowledge](#tacit-knowledge)
  - [outliers](#outliers)
  - [Others](#others)

## Knowledge base

A knowledge base (KB) is a technology used to store complex structured and unstructured information used by a computer system.
The initial use of the term was in connection with expert systems; which were the first knowledge-based systems.

## Knowledge graph

A knowledge graph is a knowledge base that uses a graph-structured data model or topology to integrate data. 
Knowledge graphs are often used to store interlinked descriptions of entities – objects, events, situations or abstract concepts (with free-form semantics).

Since the development of the Semantic Web, knowledge graphs are often associated with linked open data projects, focusing on the connections between concepts and entities.

## Knowledge Graph Completion

Task that tries to predict any missing element of the triplet (s, p, o).

- entity prediction, when an element 's' or 'o' is missing : (?, p, o) or (s, p, ?)
- relation prediction, when p is missing : (s, ?, o)
- triplet classification, when an algorithm recognizes whether a given triplet (s, p, o) is correct or not. 

There is another way of describing it : atoms

An atom with 
- two variables is called **unbounded**
- one variable is called **bounded**
- no variable is called **grounded** (also called just **statement**)

### KGC Techniques

- entity resolution (ER), 
- probabilistic soft logic (PSL)
- knowledge graph embedding (KGE)
- structured embedding
- neural tensor network
- semantic matching energy model
- latent factor model
- TransE (2013) (same space (k = l), $\vert\vert v_h - v_t +v_r\vert\vert^2_2$)
- TransH (2014) (projection on hyperplane containing $v_r$ : $\vert\vert v_{h\perp} - v_{t\perp} +v_r\vert\vert^2_2$)
- CTransR (2015) (projection matrix)
- TransD


## Linked (open) data

In computing, linked data is structured data which is interlinked with other data so it becomes more useful through semantic queries.
It builds upon standard Web technologies such as HTTP, RDF and URIs, but rather than using them to serve web pages only for human readers, it extends them to share information in a way that can be read automatically by computers.

## Ontology

> A set of concepts and categories in a subject area or domain that shows their properties and the relations between them.

An ontology encompasses a representation, formal naming and definition of the categories, properties and relations between the concepts, data and entities that substantiate one, many, or all domains of discourse.

More simply, an ontology is a way of showing the properties of a subject area and how they are related, by defining a set of concepts and categories that represent the subject. 


## Tacit Knowledge

> "we can know more than we can tell." 
> 
> Polanyi, Michael (1966), The Tacit Dimension

Tacit knowledge or implicit knowledge (as opposed to formal, codified or explicit knowledge) is the kind of knowledge that is difficult to transfer to another person by means of writing it down or verbalizing it. 

For example, that London is in the United Kingdom is a piece of explicit knowledge that can be written down, transmitted, and understood by a recipient. 

However, the ability to speak a language, ride a bicycle, knead dough, play a musical instrument, or design and use complex equipment requires all sorts of knowledge which is not always known explicitly, even by expert practitioners, and which is difficult or impossible to explicitly transfer to other people.


## outliers

Sometimes a dataset can contain extreme values that are outside the range of what is expected and unlike the other data.

These are called outliers and often machine learning modeling and model skill in general can be improved by understanding and even removing these outlier values.


## Others
link prediction :

embedding

latent-factor:

horn rule

entity centric IR

statement or fact : in a KG, a statement is a tuple (s,p,o) with subject, predicate and object. this could also be written as p(s,o)
