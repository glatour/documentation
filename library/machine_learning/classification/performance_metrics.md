# Performance Metrics

- [Performance Metrics](#performance-metrics)
  - [Introduction & classification](#introduction--classification)
  - [Confusion matrix](#confusion-matrix)
  - [Classification accuracy](#classification-accuracy)
  - [(Cohen) Kappa statistic](#cohen-kappa-statistic)
  - [Precision](#precision)
  - [Specificity](#specificity)
  - [Sensitivity or Recall](#sensitivity-or-recall)
  - [F1-score](#f1-score)
  - [ROC](#roc)
  - [AUC](#auc)
  - [Kendall rank correlation coefficient](#kendall-rank-correlation-coefficient)

## Introduction & classification

Three families of metrics :

- Metrics based on a threshold and a qualitative understanding of
error
- Metrics based on a probabilistic understanding of error, i.e. measuring the deviation from the true probability
- Metrics based on how well the model ranks the examples

Here is the repartition (and links) of the different metrics

- Threshold & qualitative
  - [accuracy](#classification-accuracy)
  - [precision](#precision)
  - [specificity](#specificity)
  - [sensitivity](#sensitivity-or-recall)
  - kappa statistic
  - macro average arithmetic
  - macro average geometric
  - [F-score](#f1-score)
  - Mean F-measure
- Probabilistic
  - MSE
  - MAE
  - Log-loss
  - Calibration Loss
  - Calibration by Bins
  - profit
  - MPR
  - MAPR
  - PAUC
- Ranking
  - AU1U
  - AUNU
  - AUNP
  - AU1P
  - [ROC](#roc)
  - [AUC](#auc)
  - MRR
  - Hit at k
  - [Kendall rank correlation coefficient](#kendall-rank-correlation-coefficient)
- in-between probabilistic and ranking
  - SAUC



- - -

## Confusion matrix


It's not a metric itself but it used a lot.

The idea is to create a double entry table, with predicted values in rows and actual values in columns.

With this kind of presentation, it's really easy to spot the true positive, the false positive, the true negative and the false negative.


## Classification accuracy

The number of correct predictions divided by the total number of predictions.

Correct predictions means true positive + true negative

## (Cohen) Kappa statistic

A way of measuring agreement between 2 classifiers.


I do not understand how this is used in practice.


## Precision

true positive / (true positive + false positive)


The idea with the precision is to measure how well the predicted positives instances are correct.


## Specificity

true negative rate.

true negative / (true negative + false positive)


## Sensitivity or Recall

true positive rate.

true positive / (true positive + false negative)


The idea with the recall is to measure how well the model has correctly classified positive instances. (And how many it has missed)


## F1-score

This metric combine the precision and recall

$F_1$ = 2 * precision * recall / (precision + recall)

This can be generalized as $F_{\beta} = (1 + \beta^2) \cdot \frac{precision \cdot recall}{\beta^2 \cdot precision + recall}$

There is always a tradeoff between precision and recall.


## ROC

Receiver Operating Characteristic curve 

It's the curve obtained by plotting, for each meta-parameter of the model that provides a different confusion matrix, the true positive rate (sensitivity) in relation with the false positive rate (specificity).

![roc_curve](.images/roc.png)


## AUC

Area Under the Curve

This is the area created by the ROC curve.

The bigger the better.

![auc_roc](.images/roc_auc.png)

## Kendall rank correlation coefficient

In statistics, the Kendall rank correlation coefficient, commonly referred to as Kendall's τ coefficient, is a statistic used to measure the ordinal association between two measured quantities. 

A τ test is a non-parametric hypothesis test for statistical dependence based on the τ coefficient. 

![kendall](.images/kendall.png)