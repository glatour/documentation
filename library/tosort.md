# Link prediction in knowledge graph

link prediction = relational learning


link prediction in knowledge graph : 2 paradigms :
- symbolic methods
- approaches based on embeddings

## Symbolic methods

### References
- Chen, Y., Wang, D.Z., Goldberg, S.: ScaLeKB: Scalable Learning and In-
ference over Large Knowledge Bases. VLDB Journal 25(6) (2016)
- Lajus, J., Galárraga, L., Suchanek, F.: Fast and Exact Rule Mining with
AMIE 3. In: ESWC: The Semantic Web. pp. 36–52 (2020)
- Lao, N., Mitchell, T., Cohen, W.W.: Random Walk Inference and Learn-
ing in A Large Scale Knowledge Base. In: EMNLP. pp. 529–539 (2011),
https://www.aclweb.org/anthology/D11-1049
- Ortona, S., Meduri, V.V., Papotti, P.: Robust Discovery of Positive and
Negative Rules in Knowledge Bases. In: ICDE (2018)

### Techniques

- The PRA algorithm uses paths as features for link prediction via logistic regression.
- AMIE
- Rudik
- OP
- QuickFoil


## Embeddings

Vectorial representation of an entity.

### References

- Bordes, A., Usunier, N., Garcia-Duran, A., Weston, J., Yakhnenko, O.:
Translating Embeddings for Modeling Multi-relational Data. In: Advances
in Neural Information Processing Systems 26, pp. 2787–2795 (2013)
- Lin, Y., Liu, Z., Sun, M., Liu, Y., Zhu, X.: Learning Entity and Relation
Embeddings for Knowledge Graph Completion. In: AAAI (2015)
- Nickel, M., Rosasco, L., Poggio, T.: Holographic Embeddings of Knowledge
Graphs. In: AAAI. pp. 1955–1961 (2016)
- Nickel, M., Tresp, V.: Tensor Factorization for Multi-relational Learning.
In: Machine Learning and Knowledge Discovery in Databases (2013)
- Socher, R., Chen, D., Manning, C.D., Ng, A.Y.: Reasoning with Neural
Tensor Networks for Knowledge Base Completion. In: NIPS (2013)
- Trouillon, T., Welbl, J., Riedel, S., Gaussier, E., Bouchard, G.: Complex
Embeddings for Simple Link Prediction. In: ICML. pp. 2071–2080 (2016)
- Wang, Z., Zhang, J., Feng, J., Chen, Z.: Knowledge Graph Embedding by
Translating on Hyperplanes. In: AAAI (2014)
- Yang, B., Yih, S.W.t., He, X., Gao, J., Deng, L.: Embedding Entities and
Relations for Learning and Inference in Knowledge Bases. In: ICLR (2015) 

### Techniques 

- RESCAL is a bilinear approach that stores pairwise interactions between
entity embeddings in a tensor $W$ with one slice per predicate.
- DistMult builds upon the same philosophy but enforces $W_p$ to be a diagonal matrix in order to reduce the number of parameters in the model.
- Complex defines $W_p$ as a complex diagonal matrix.
- HolE models predicates as vectors and replaces the tensor (matrix) product by the circular correlation operator.
- MLPs techniques model the interactions between the latent representation of entities and predicates.

## Hybrid

it exists.

- Iter


# Horn rule

Using Horn rule to mine explanations from black boxed link predictors :

- Ruschel, A., Gusmão, A.C., Polleti, G.P., Cozman, F.G.: Explaining Completions Produced by Embeddings of Knowledge Graphs. In: ECSQARU. pp. 324–335 (2019). https://doi.org/10.1007/978-3-030-29765-7 27
- Yang, B., Yih, S.W.t., He, X., Gao, J., Deng, L.: Embedding Entities and Relations for Learning and Inference in Knowledge Bases. In: ICLR (2015)
  
Conjunction of atoms.

- a rule is **safe** when head variables occur also in the body
- a rule is **closed** when safe and each variable occurs in at least two atoms



# Explainability

## Criterions to measure the explainability

- scope
  - global
  - local
- fidelity
  - AUC-ROC
  - MRR
  - coverage (proportion of the graph for which an explanation/rule can be provided)