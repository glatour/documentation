# Questions

## Luis

- What are the differences between 
  -  link prediction
  -  rule mining
  -  KGC
  > **Link prediction** : task consisting of predicting if there is a link between two entities. (take a KG and train a model to check if there should be a relation between two nodes of the KG.)
  > It's possible to use a model that is based on rules to find new links, but not mandatory.
  > 
  > **Rule mining** : task consisting of extracting rules from a KG. (automatic, unsupervised)
  > 
  > **KGC** : task consisting of adding new entities/relations in the KG. It's the most englobing task, the final task.
  > It's possible to use link prediction to complete a KG but it's not restricted to this technique.


- What really is the "ontology" of a knowledge graph ?
  > Modélisation de la réalité d'un domaine de connaissance
  >
  > ontology = KG qui possède un schéma strict ("habite à" & "réside à" sont similaires)
  > 
  > ontology $\implies$ KG 


- "In real world, we can define a priori $k$, $l$, $f_s$ but not $f_E$ and $f_R$." ([article](https://towardsdatascience.com/embedding-models-for-knowledge-graph-completion-a66d4c01d588)) : are those meta parameters ?
  > Is this question pertinent ?

- What if there is a clash between horn rules (parent(x,z) $\land$ nationality(z,y) $\implies$ nationality(x,y) with father belgian & mother french) ? how to detect/solve the conflict ? What about clash between different methods of rules mining ? how to measure pertinence of rule ?
  > "Open World Assumption" : if a fact is absent from the KG, it's not de facto false.
  > 
  > Here it's not the best example because you can have multiple nationalities, but if we assume that the rule implies the birth place.
  > 
  > There is a metric that is used to measure the quality of the rule : "confidence"
  > 
  > Multiple confidence metrics can be created, depending on the world assumption (open, closed,..)
  > 
  > The confidence : $\frac{n}{t}$ with $n$ the number of correct predictions done by the rule and $t$ the total number of predictions made by the rule.

- Are self-predicate allowed ? ( $p(s,s)$)
  > yes but uncommon

- What methods are used to measure how hollow is a KG ? (what methods are used to assert that a KG is full ?)
  > Always linked to a specific relation.
  > 
  > Statistical methods are used to calculate this.

- I have like a hard threshold after a few hours of high-level reading. Is there anything else I could *do* when I can't read anymore but still have to work ?
  > Write summary when reading.

- Luis said that some benchmark datasets were biased due to redundancy.
What are the impacted datasets & how was exposed this duplication ?
  > "fb15k" has duplicates relations (*citizenship* & *citizen_of*) that could create rules like $p(s,o) \implies p^{-1}(o,s)$
  > > Akrami, F., Saeef, M.S., Zhang, Q., Hu, W., Li, C.: Realistic Re-Evaluation
  of Knowledge Graph Completion Methods: An Experimental Study. In:
  SIGMOD. p. 1995–2010 (2020). https://doi.org/10.1145/3318464.3380599
  >
  > > Meilicke, C., Fink, M., Wang, Y., Ruffinelli, D., Gemulla, R., Stuck-
  enschmidt, H.: Fine-Grained Evaluation of Rule and Embedding-Based Systems for Knowledge Graph Completion. In: ISWC. pp. 3–20 (2018).
  https://doi.org/10.1007/978-3-030-00671-6 1

- If the rules are mined *after* the new links are predicted, are we sure that we are really explaining the function that created new links ?
- Is there a way to see the impact of the *new* links on the mined rules (new rules, different weights, ...) ?

## Benoît

## Both

- How to manage the evolution of a KG over time ?
    - duration label on relations ? (true between `timestamp` & `now`) ??
> triplets + time labels
> 
> Quadruplets (triplets + IRI linking to entity that gives metadata) (Reification)

## Guillaume

- aller voir RESCAL & TRans-E
- aller voir regression logistique
- have to re-read the context $C$ in `eswc_paper.pdf`.