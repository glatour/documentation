---
marp: true
paginate: true
backgroundImage: url('images/hero-background.jpg')
footer: 'UNamur:white_heart: INRIA - Guillaume Latour'
---
<!-- _paginate: false -->
# 📄 Explaining Embedding-based Models for Link Prediction in Knowledge Graphs


> Guillaume Latour
> Supervisors: Benoît Frénay, Luis Galárraga & Danai Symeonidou 
> 
---
# Contents

1. Preliminaries
   1. KG, fact, atom, Horn rules
   2. Graph completion
   3. Interpretability
2. Algorithm
3. Experiments
4. Conclusion

--- 
# 1. Preliminaries
2. Algorithm
3. Experiments
4. Conclusion
 
---
<!-- header: "Preliminaries" -->
<style scoped>
  p {
    font-size: 0.8em;
    color: grey;
  }
</style>
# Knowledge Graph

![left](images/example_kg_1.png) ![right](images/example_kg_2.png)

> Images taken from [1]

<!-- 
Souvent ces graphes là sont incomplets.

Donc les faits qui sont manquants sont soit faux, soit vrais mais absents.
-->
---
# Facts / Statements

- marriedTo(Barack, Michelle)
- marriedTo(Michelle, Barack)
- hasChild(Michelle, Sasha)
- hasChild(Michelle, Malia)

**Atom** = statement with variable(s)
- marriedTo(Barack, **x**)
![bg right w:100%](images/example_kg_2.png)


---
# Horn rules
- marriedTo(**x**, **y**) $\implies$ marriedTo(**y**, **x**)
- marriedTo(**x**, *y*) $\land$ hasChild(*y*, **z**) $\implies$ hasChild(**x**, **z**)

Horn rules can be extracted from a KG with an associated score.

- KG Analysis
- Graph completion

![bg right:29% fit ](images/example_kg_1.png)

---
# Graph completion

Tasks aiming at finding missing relations in a KG

- link prediction
- triplet classification

![bg right:40% fit](images/example_kg_2.png)


---
<!-- _header: '' -->
<!-- _footer: '' -->
<style scoped>
  p {
    font-size: 0.8em;
    color: grey;
    padding-left: 800px;
    padding-top: 550px;
  }
</style>
![bg fit](images/taxonomy_link_predictor.png)
exhaustive survey in [2]

<!--
Les méthodes à base d'embedding plongent les entités et les prédicats dans un espace latent.

Cele permet de calculer plus efficacement les proximités éventuelles entre les entités et/ou les relations.
-->

---
![bg right:38% fit](images/representation_link_predictor.jpg)
## Symbolic methods
- (+) human readable explanation
- (-) not scalable


## Embedding based methods
- (+) scalable
- (-) not intrinsically explainable
<!--
Les modèles sont entrainés de telle sorte que les faits vrais aient de meilleurs scores que les faits faux.
-->

---
# Interpretability


“Interpretability is the degree to which a human can understand
the cause of a decision.”
<!--
Les règles de Horn sont interprétables.
-->

---
# Motivation
![bg right:30% fit](images/both_kg_example.jpg)

A link predictor $f$ produced this statement:
- hasChild(Barack, Sasha)

The framework produced this explanation:
- marriedTo(**x**, *y*) $\land$ hasChild(*y*, **z**) $\implies$ hasChild(**x**,**z**)

> Would you trust this link prediction ?
> 
> Was the provided explanation useful for your trust decision-making ?

---
![bg right:30% fit](iamges/../images/representation_link_predictor_1.jpg)
# Link predictor

A *link predictor* is function $f: \Omega (\mathcal{K}) \rightarrow \mathbb{R}$ that assigns scores to the statements in the *potential set* of a KG.

For *embedding based linked predictors* $\rightarrow$ $f = \hat{f} \circ h$
with
- $h: \Omega(\mathcal{K}) \rightarrow \mathbb{R}^k$ (conversion from facts to $k$-dimensional vectors)
- $\hat{f}: \mathbb{R}^k \rightarrow \mathbb{R}$

$\hat{f}$ = black box that we want to explain
$h$ = embedding function

---
![bg right:30% fit](iamges/../images/representation_link_predictor_2.jpg)
# Link predictor

A *link predictor* is function $f: \Omega (\mathcal{K}) \rightarrow \mathbb{R}$ that assigns scores to the statements in the *potential set* of a KG.

For *embedding based linked predictors* $\rightarrow$ $f = \hat{f} \circ h$
with
- $h: \Omega(\mathcal{K}) \rightarrow \mathbb{R}^k$ (conversion from facts to $k$-dimensional vectors)
- $\hat{f}: \mathbb{R}^k \rightarrow \mathbb{R}$

$\hat{f}$ = black box that we want to explain
$h$ = embedding function

---
![bg right:30% fit](iamges/../images/representation_link_predictor_3.jpg)
# Link predictor

A *link predictor* is function $f: \Omega (\mathcal{K}) \rightarrow \mathbb{R}$ that assigns scores to the statements in the *potential set* of a KG.

For *embedding based linked predictors* $\rightarrow$ $f = \hat{f} \circ h$
with
- $h: \Omega(\mathcal{K}) \rightarrow \mathbb{R}^k$ (conversion from facts to $k$-dimensional vectors)
- $\hat{f}: \mathbb{R}^k \rightarrow \mathbb{R}$

$\hat{f}$ = black box that we want to explain
$h$ = embedding function

---
![bg right:30% fit](iamges/../images/representation_link_predictor.jpg)
# Link predictor

A *link predictor* is function $f: \Omega (\mathcal{K}) \rightarrow \mathbb{R}$ that assigns scores to the statements in the *potential set* of a KG.

For *embedding based linked predictors* $\rightarrow$ $f = \hat{f} \circ h$
with
- $h: \Omega(\mathcal{K}) \rightarrow \mathbb{R}^k$ (conversion from facts to $k$-dimensional vectors)
- $\hat{f}: \mathbb{R}^k \rightarrow \mathbb{R}$

$\hat{f}$ = black box that we want to explain
$h$ = embedding function

---
# Explanation

An *explanation* for the latent-feature-based link predictor $\hat{f}$ is a function $g: \mathbb{R}^{k'} \rightarrow \mathbb{R}$ that approximates $\hat{f}$.

>:warning: dimension is $k'$

<!--
Donc mon travail a été le design et l'implémentation d'un algorithme qui fournit des explications en approximants le link predicteur black-box.
-->

---
<!-- header: '' -->
1. Preliminaries
# 2. Algorithm
3. Experiments
4. Conclusion

---
<!-- header: "Algorithm" -->
# Overview
We used a **surrogate model**, which makes it *model agnostic*

The output of our framework are **explanations** (Horn rules), mined by AMIE [3]

---
# Algorithm

With 
- a link predictor $\hat{f}: \mathbb{R}^k \rightarrow \mathbb{R}$
- a KG $\mathcal{K} = \mathcal{K}^+ \cup \mathcal{K}^-$
- a context $C$, filled with statements within a given scope

1) Call $\hat{f}$ on each context element $\rightarrow$ create $\hat{\mathcal{K}}$
2) Mine rules $B \implies p(x,y)$ on $\mathcal{K} \cup \hat{\mathcal{K}}$ 
3) Create matrix with facts as instances, rules weights as features and label as target
4) Use logistic regression to train a surrogate model on that matrix


<!--
K- est un ensemble de faits qui sont faux.
Ceux-ci ont été généré via une tâche de Negative Sampling.
-->

---
# Contexts

## Global (around a predicate)

$C$ is populated with facts containing the same predicate.

## Local (around a statement)

$C$ is populated with same facts as target, with variations on its head and tail.
For each $p(s,o) \in \mathcal{K}$, $C_{p(s,o)} = \{p(s',o) \in \mathcal{K} \} \cup \{p(s, o') \in \mathcal{K} \}$

---
# Evaluation
- **Fidelity** How good the explanation approximates the black box model ?
- **Coverage** Ratio of facts for which an explanation is provided
- **Link prediction**
  - MRR (Mean Reciprocal Rank)
- **Triplet classification**
  - Accuracy
<!--
La MRR a été inventée pour des tâche d'Information Retreival
-->

---
<!-- header: '' -->
1. Preliminaries
2. Algorithm
# 3. Experiments
4. Conclusion

---
<!-- header: 'Experiments' -->
# Embedding model

We used TransE [4] to perform our experiments.

Any model can be used, and is considered as a black-box by our framework.

---
# Datasets
We used state of the art datasets to provide comparable metrics of our framework.

- fb15k-237 (subset of Freebase)
- wn18rr (subset of WordNet)

| Dataset   | Predicates | Entities | Train. facts | Valid. facts | Test. facts |
| --------- | ---------- | -------- | ------------ | ------------ | ----------- |
| wn18rr    | 11         | 40 943   | 86 865       | 3 034        | 3 134       |
| fb15k-237 | 237        | 15 541   | 272 115      | 17 535       | 20 466      |

---
# Results overview
Average of global contexts
| Dataset   | Fidelity | MRR   | Coverage |     | Accuracy (black-box) |
| --------- | -------- | ----- | -------- | --- | -------------------- |
| wn18rr    | 0.487    | 0.105 | 0.013    |     | 0.536                |
| fb15k-237 | 0.941    | 0.079 | 0.427    |     | 0.506                |
<!-- 
Au plus la métrique est élevée au mieux.

Mauvaise Accuracy = 0.5, autant qu'un pile ou face.


On peut expliquer les mauvais résultats de la boite noire en se disant que TransE fonctionne bien pour les relations 1-1, mais pas pour les relations 1-N ou N-N.
-->

---
# Specific results
| $C$ | Target relation/instance              | Fidelity | MRR   | Coverage |     | Accuracy |
| --- | ------------------------------------- | -------- | ----- | -------- | --- | -------- |
| g   | /person/profession                    | 0.975    | 0.002 | 0.6 |     | 0.510    |
| g   | /film/produced_by                     | 0.916    | 0.285 | 0.6      |     | 0.611    |
| l   | Mike Figgis /profession Film director | 0.980    | 0.25  | 0.602    |     | 0.580    |
| l   | Marilyn Manson /profession Actor      | 0.987    | 0.01  | 0.603    |     | 0.512    |


---
# Output example

- ($a$  /film/film/produced\_by Adam Sandler)
  $\implies$ ($a$ /film/film/produced\_by Jack Giarraputo)
- (Rob Schneider /film/actor/film./film/performance/film  $a$)
  $\implies$ ($a$ /film/film/produced\_by Jack Giarraputo)

<!--
Cette explication nous permet de voir que TransE a appris que Jack Giarraputo avait un acteur et un producteur préféré.
-->

---
# Detecting issue in data

- (Nicole Richie  /has\_celebrity\_friend  $b$) $\land$ ($a$  /has\_celebrity\_friend  $b$)
  $\implies$ ($a$  /people/person/profession  Actor)
- (Nicole Richie  /has\_celebrity\_friend  $b$) $\land$ ($b$  /has\_celebrity\_friend  $a$)
  $\implies$ ($a$  /people/person/profession  Actor)
- ($b$  /canoodled  $a$) $\land$ ($b$  /romantic\_relationship  Nicole Richie)
  $\implies$ ($a$  /people/person/profession  Actor)
- ($b$  /canoodled  $a$) $\land$ (Nicole Richie  /romantic\_relationship  $b$)
  $\implies$ ($a$  /people/person/profession  Actor)

---
# Usage
- debug link predictor
- detect biases in the data
  - symmetric rules

--- 
<!-- header: '' -->
1. Preliminaries
2. Algorithm
3. Experiments
# 4. Conclusion

---
<!-- header: 'Conclusion' -->
# Acquired Skills

- discover KGs, link prediction, AMIE, *etc.*
- design and implement a surrogate model (https://gitlab.inria.fr/glatour/geebis)
- contribute in `torchKGE` library  (https://github.com/torchkge-team/torchkge)
- launch experiments on a supercalculator (igrida)
- live side by side with researchers

---
# Conclusion

Our framework is able to simulate and explain a black box behaviour for link prediction.

Its performance vary depending on context.

---
# Perspectives
#### Models
We have used **TransE**.
It would be interesting to see the explanation of different models predictions.

#### Proxy
We trained our surrogate via **logistic regression**.
Any other model allowing an explanation from it's weighted features could be used.

#### Context
We have introduced **local** and **global** scopes.
Different definitions or other scopes could be proposed and compared.

---
<!-- header: '' -->
<style scoped>
  p {
    font-size: 0.8em;
  }

</style>
# Thank you for your attention:smiley:

### References
[1] Jonathan Lajus, “Fast, Exact, and Exhaustive Rule Mining in Large Knowledge Bases,” Ph.D. dissertation.
[2] M. Wang, L. Qiu, and X. Wang, “A survey on knowledge graph embeddings for link prediction,” Symmetry, vol. 13, no. 3, 2021.
[3] L.A.Galárraga,C.Teflioudi,K.Hose,andF. Suchanek,“AMIE: association rule mining under in-complete evidence in ontological knowledge bases,” in Proceedings of the 2nd international conference on World Wide Web - WWW ’13. Rio de Janeiro, Brazil: ACM Press, 2013, pp. 413–422.
[4] A. Bordes, N. Usunier, A. Garcia-Duran, J. Weston, and O. Yakhnenko, “Translating Embeddings for Modeling Multi-relational Data,” p. 9, 2013.

---
# Code

![fit](images/gitlab.png)

--- 
<!-- header: '' -->
<!-- _paginate: false -->
<!-- backgroundImage: url('images/thats_all_folks.jpg') -->
