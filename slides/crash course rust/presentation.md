---
marp: true
paginate: true
backgroundImage: url('images/hero-background.jpg')
footer : 'UNamur :white_heart: INRIA - Guillaume Latour'
---
<!-- _paginate: false -->
<!-- _class: 'lead' -->
![bg right:75% fit](images/rust_logo.png)
![bg fit](images/rusty.png)
# Rust

---
![bg right:51% fit](images/rust_book_screen.png)
# Book
https://doc.rust-lang.org/stable/book/

---
<!-- _header: '' -->
<!-- _paginate: false -->
# Installation & "Hello, world!"

---

<!-- header: 'First steps' -->
# Installation
## Linux/Mac
```bash
curl --proto '=https' --tlsv1.2 https://sh.rustup.rs -sSf | sh
```

## Windaube
- download and execute installer from https://forge.rust-lang.org/infra/other-installation-methods.html
- use a package manager
  - chocolatey
  - scoop

---
# Installed softwares

- rustc (compilator)
- rustup (toolchain manager)
- cargo (tools)

---
# Cargo

- `new` generate new project (`--lib` for libraries)
- `check` quickly check if the project compile without generating executable
- `build` compile project (`--release` to compile a release executable)
- `run` compile and run project
- `test` launch tests

---
# Hello, world!

```bash
cargo new lacodam
cd lacodam
cargo run
```
```
❯ cargo run
   Compiling lacodam v0.1.0 (/mnt/MobyDisk/Documents/.tmp/playground/rust/lacodam)
    Finished dev [unoptimized + debuginfo] target(s) in 1.06s
     Running `target/debug/lacodam`
Hello, world!
```

---
# Files generated
```
. (lacodam)
├── Cargo.lock
├── Cargo.toml
├── src
│   └── main.rs
└── target
    ├── [...]
    └── debug
        ├── [...]
        └── lacodam
```

---
`src/main.rs`
```rust
fn main() {
    println!("Hello, world!");
}
```

`Cargo.toml`
```toml
[package]
name = "lacodam"
version = "0.1.0"
authors = ["Guillaume <Tazoeur> Latour <g0latour@gmail.com>"]
edition = "2018"

# See more keys and their definitions at https://doc.rust-lang.org/cargo/reference/manifest.html

[dependencies]
```

---
# By hand

```bash
printf 'fn main() {\n prinln\!("Hello, world\!");\n }\n' > main.rs
rustc main.rs
./main
```

---
<!-- _header: '' -->
<!-- _paginate: false -->
# Let me see some code !

---
<!-- header: 'Small game' -->
```rust
use rand::Rng; // extern crate (need dependency in `Cargo.toml`)
use std::cmp::Ordering;
use std::io;

fn main() {
    println!("Guess the number!");

    let secret_number = rand::thread_rng().gen_range(1..101);

    loop { // while true
        println!("Please input your guess.");

        let mut guess = String::new();

        io::stdin()
            .read_line(&mut guess)
            .expect("Failed to read line");

        let guess: u32 = match guess.trim().parse() {
            Ok(num) => num,
            Err(_) => continue,
        };

        println!("You guessed: {}", guess);

        match guess.cmp(&secret_number) {
            Ordering::Less => println!("Too small!"),
            Ordering::Greater => println!("Too big!"),
            Ordering::Equal => {
                println!("You win!");
                break;
            }
        }
    }
}
```

---
# Variable declaration

```rust
let my_variable: u32 = 42;
let mut my_mutable_variable = String::new();
```

```rust
const MAX_VALUE: u32 = 100_000; // always annotate type for constants
```

---
# Range
```rust
let secret_number = rand::thread_rng().gen_range(1..101);
```

```rust
for i in 1..=10 {
    println!("number #{}", i);
}
```

```rust
for i in (1..=10).step_by(2) {
    prinln!("even number #{}", i);
}
```

---
# Matching
```rust
let guess_int: u32 = match guess.trim().parse() {
    Ok(num) => num,
    Err(_) => continue,
};
```

```rust
match guess_int.cmp(&secret_number) {
    Ordering::Less => println!("Too small!"),
    Ordering::Greater => println!("Too big!"),
    Ordering::Equal => {
        println!("You win!");
        break;
    }
}
```

---
# Shadowing

```rust
let mut guess = String::new();

io::stdin()
    .read_line(&mut guess)
    .expect("Failed to read line");

let guess: u32 = match guess.trim().parse() {
    Ok(num) => num,
    Err(_) => continue,
};
```

```rust
let x = 5;
let x = x + 1; // without the keyword `let`, this would not compile
let x = x * 2;
```

---
<!-- _header: '' -->
<!-- _paginate: false -->
# Data Types

---
<!-- header: 'Data Types' -->
# Data Types

Rust is a ***statically* typed** language, which means that it must know the types of all variables at compile time.

- ***Scalar* types** represents a single value (integer, floating-point, boolean and character)
- ***Compound* types** can group multiple values into one type (tuple, array)

---
# Integer Types
| Length  | Signed  | Unsigned |
| :-----: | :-----: | :------: |
|  8-bit  |  `i8`   |   `u8`   |
| 16-bit  |  `i16`  |  `u16`   |
| 32-bit  |  `i32`  |  `u32`   |
| 64-bit  |  `i64`  |  `u64`   |
| 128-bit | `i128`  |  `u128`  |
|  arch   | `isize` | `usize`  |

---
# Floating-point Types

`f32` and `f64`

```rust
let number: f32 = 42.23;
let big_number: f64 = 123.456;
```

default inference cast to `f64`

---
# Boolean Type
Can only be `true` or `false`
```rust
let t = true;
let v: bool = false; // explicit type annotation
```

---
# Character Type

Rust `char` type is 4 bytes in size and represents a Unicode Scalar Value.
This include accented letters; Chinese, Japanese and Korean characters; emoji.

```rust
let c = 'z'; // single quotes around a char !
let z = 'ℤ';
let heart_eyed_cat = '😻';
```

from `U+0000` to `U+D7FF` and `U+E000` to `U+10FFFF` inclusive.

---
# Tuple Type

Group a combination of different types inside one type.

```rust
let tup: (i32, char, f64) = (-42, 'c', 42.69);
let position = (1, -1);

let (x, y, z) = tup;
let abscissa = position.0;
```

---
# Array Type
Fixed length collection having the same type.

```rust
let tab = [1, 2, 3, 4, 5];
let months = ["January", "February", "March", "April", "May", "June", "July",
              "August", "September", "October", "November", "December"];
let a: [i32; 5]; // [type; size]
a = [3; 5];      // [3, 3, 3, 3, 3]

println!("{}", months[0]); // "January"
```

Of course, trying to access to an out-of-bounds items will make rust **panic**

---
# String type

Previously seen data types are stored on the **stack**. 
We want to look at data that is stored on the **heap** to explore how Rust knows when to clean up that data.

```rust
let h = "Hello"; // string literal, hardcoded, known size
let mut s = String::from(h); // "real" String

s.push_str(", world!"); // appends a literal to a String

println!("{}", s);
```

---
<!-- _header: '' -->
<!-- _paginate: false -->
# Functions

---
<!-- header: 'Functions' -->
# Main

The `main` function is the entry point of the program.

```rust
fn main() {
    println!("Hello, world!");

    another_function();
}

fn another_function() {
    prinln!("Another function.");
}
```

---
# Function signature & expression return

By default, a function return the last expression.
```rust
fn addition(a: i32, b: i32) -> i32 {
    a + b
}
```

The line `a + b` does not end with a semicolon, so it's an **expression**.
Lines ending with a semicolon are **statements**.

It's possible to return early with the `return` keyword.

---
# Forgot semicolon

```rust
error[E0308]: mismatched types
 --> src/main.rs:8:32
  |
8 | fn addition(a: i32, b: i32) -> i32 {
  |    --------                    ^^^ expected `i32`, found `()`
  |    |
  |    implicitly returns `()` as its body has no tail or `return` expression
9 |     a + b;
  |          - help: consider removing this semicolon
```

---
<!-- _header: '' -->
<!-- _paginate: false -->
# Control Flow

---
<!-- header: 'Control Flow' -->
# `if` expressions

```rust
let n = 3;

if n < 5 {      // parentheses optional
    println!("Condition is true");
} else {
    prinln!("Condition is false");
}
```

```rust
let a = if n < 5 {
    5
} else if n < 10 {
    10  // same type as the first expression !
} else {
    100
};      // end a statement with a semicolon

let b = if n < 5 { 5 } else { 100 };
```

---
# `if` conditions must be `bool`

```rust
let n = 3;
if n {
    println!("number exist");
}
```
```
error[E0308]: mismatched types
 --> src/main.rs:4:8
  |
4 |     if number {
  |        ^^^^^^ expected `bool`, found integer
```

---
# Loops with `loop`

Infinite loop.

```rust
let mut counter = 0;
loop {
    counter += 1;
    println!("Never gonna give you up");
    println!("Never gonna let you down");

    if counter >= 10 {
        break counter * 2; // return value from a loop
    }
}
```

---
# Loops with `while`

```rust
let mut number = 3;
while number > 0 {
    println!("{}!", number);
    number -= 1;
}
println!("end of program");
```

---
# Loops with `for`

```rust
let tab = ['a', 'b', 'c', 'd', 'e', 'f', 'g'];

for element in tab.iter() {
    println!("{}", element);
}

for (index, element) in tab.iter().enumerate() {
    println!("{} => {}", index, element);
}
```

```rust
for i in (1..=10).rev() {
    println!("{}", i);
}
```

---
<!-- _header: '' -->
<!-- _paginate: 'false' -->
# :star: Ownership

---
<!-- header: 'Ownership' -->
# Ownership

Ownership is Rust’s most *unique feature*, and it enables Rust to make **memory safety** guarantees **without** needing **a garbage collector**.

Ownership is a **new concept** for many programmers $\rightarrow$ it does take time to get used to it.

---
# Ownership rules


- Each value in Rust has a variable that’s called its owner.
- There can only be one owner at a time.
- When the owner goes out of scope, the value will be dropped.

---
# Scope

```rust
{                      // s is not valid here, it’s not yet declared
    let s = "hello";   // s is valid from this point forward

    // do stuff with s
}                      // this scope is now over, and s is no longer valid
```

> In C++, this pattern of deallocating resources at the end of an item lifetime is known as Resource Acquisition Is Initialization (RAII)

---
# Variables and data interaction : Copy

```rust
let x = 5;
let y = x;
```

1. Bind the value `5` to the variable `x`
2. make a copy of `x` and bind it to `y`

We now have 2 variables `x` and `y`, both equals to `5`.

---
![bg right:40% fit](images/variable_creation.svg)
# Variables and data interaction : Move

```rust
> let s1 = String::from("hello");
let s2 = s1;
```

---
![bg right fit](images/variable_move.svg)
# Variables and data interaction : Move

```rust
let s1 = String::from("hello");
> let s2 = s1;
```

---
# Variables and data interaction : Move
```rust
let s1 = String::from("hello");
let s2 = s1;

println!("{}, world!", s1);
```

```
error[E0382]: borrow of moved value: `s1`
 --> src/main.rs:5:28
  |
2 |     let s1 = String::from("hello");
  |         -- move occurs because `s1` has type `String`, which does not implement the `Copy` trait
3 |     let s2 = s1;
  |              -- value moved here
4 | 
5 |     println!("{}, world!", s1);
  |                            ^^ value borrowed here after move
```

---
![bg right fit](images/variable_clone.svg)
# Explicit clone

```rust
let s1 = String::from("hello");
let s2 = s1.clone();

println("s1 = {}, s2 = {}", s1, s2);
```

---
# Ownership in functions
```rust
fn takes_ownership(variable: String) { // the variable comes into scope
    println!("{}", variable);
} // the variable goes out of scope, and the `drop` function is called.
```

```rust
fn makes_copy(variable: i32) { // the variable comes into scope
    println!("{}", variable);
} // the variable goes out of scope and the value is freed from memory
```

---
```rust
fn main() {
    let s = String::from("hello"); // s comes into scope

    takes_ownership(s); // s value is moved into the function ...
                        // ... so it's not valid from here !

    let x: i32 = 5; // x comes into scope

    makes_copy(x);  // x value would have been moved into the function, but 
                    // i32 implement the Copy trait, so it's ok to use x
                    // afterward.
}


```

---
```rust
fn main() {
    let s = String::from("hello"); // s comes into scope

    takes_ownership(s); // s value is moved into the function ...
                        // ... so it's not valid from here !

    println!("{}", s); // won't compile !!
}
```
```
error[E0382]: borrow of moved value: `s`
 --> src/main.rs:7:20
  |
2 |     let s = String::from("hello"); // s comes into scope
  |         - move occurs because `s` has type `String`, which does not implement the `Copy` trait
3 | 
4 |     takes_ownership(s); // s value is moved into the function ...
  |                     - value moved here
...
7 |     println!("{}", s); // won't compile !!
  |                    ^ value borrowed here after move
```

---
# Return value

```rust
fn main() {
    let s1 = String::from("hello");                 // 1

    let (s2, len) = calculate_length(s1);           // 2 -> 4.2

    println!("The length of '{}' is {}.", s2, len); // 5
}

fn calculate_length(s: String) -> (String, usize) { // 3
    let length = s.len();

    (s, length)                                     // 4.1
}
```
---
<!-- _header: '' -->
<!-- _paginate: 'false' -->
# :star: Borrowing

---
<!-- header: 'Borrowing' -->
# References & Borrowing

```rust
fn main() {
    let s1 = String::from("hello");

    let len = calculate_length(&s1); // `&s1` is a reference to the value of `s1`
                                     //  but it does not own the value !

    println!("The length of '{}' is {}.", s1, len);
}

fn calculate_length(s: &String) -> usize { // s comes into scope, it's a reference 
                                           // to a String
    s.len()
} // s goes out of scope. But because it does not have ownership of what
  // it refers to, nothing happens.
```

---
![bg width:100%](images/reference_ex_1.svg)

---
Having references as parameter is called **Borrowing**.

You can create as many *immutable* references as you want !

You can only give 1 *mutable* reference at a time !

---
# Mutable borrowing
(and why rust compiler is awesome)
```rust
fn main() {
    let s = String::from("hello");
    change(&s);
}

fn change(some_string: &String) {
    some_string.push_str(", world");
}
```

```
error[E0596]: cannot borrow `*some_string` as mutable, as it is behind a `&` reference
 --> src/main.rs:8:5
  |
7 | fn change(some_string: &String) {
  |                        ------- help: consider changing this to be a mutable reference: `&mut String`
8 |     some_string.push_str(", world");
  |     ^^^^^^^^^^^ `some_string` is a `&` reference, so the data it refers to cannot be borrowed as mutable

```

---
```rust
fn main() {
    let s = String::from("hello");
    change(&s);
}

fn change(some_string: &mut String) {
    some_string.push_str(", world");
}
```
```
error[E0308]: mismatched types
 --> src/main.rs:4:12
  |
4 |     change(&s);
  |            ^^ types differ in mutability
  |
  = note: expected mutable reference `&mut String`
                     found reference `&String`
```
---

```rust
fn main() {
    let s = String::from("hello");
    change(&mut s);
}

fn change(some_string: &mut String) {
    some_string.push_str(", world");
}
```
```
error[E0596]: cannot borrow `s` as mutable, as it is not declared as mutable
 --> src/main.rs:4:12
  |
2 |     let s = String::from("hello");
  |         - help: consider changing this to be mutable: `mut s`
3 | 
4 |     change(&mut s);
  |            ^^^^^^ cannot borrow as mutable
```

---
```rust
fn main() {
    let mut s = String::from("hello");
    change(&mut s);
}

fn change(some_string: &mut String) {
    some_string.push_str(", world");
}
```
:crab: :smile:

---
# 1 mutable borrow at a time !

```rust
let mut s = String::from("hello");

let r1 = &mut s;
let r2 = &mut s;

println!("{}, {}", r1, r2);
```
> :warning: this code does not compile !
---
```
error[E0499]: cannot borrow `s` as mutable more than once at a time
 --> src/main.rs:5:14
  |
4 |     let r1 = &mut s;
  |              ------ first mutable borrow occurs here
5 |     let r2 = &mut s;
  |              ^^^^^^ second mutable borrow occurs here
6 | 
7 |     println!("{}, {}", r1, r2);
  |                        -- first borrow later used here
```

---
```rust
let mut s = String::from("hello");
let r1 = &s; // no problem
let r2 = &s; // no problem
let r3 = &mut s; // BIG PROBLEM
println!("{}, {}, and {}", r1, r2, r3);
```
```
error[E0502]: cannot borrow `s` as mutable because it is also borrowed as immutable
 --> src/main.rs:6:14
  |
4 |     let r1 = &s; // no problem
  |              -- immutable borrow occurs here
5 |     let r2 = &s; // no problem
6 |     let r3 = &mut s; // BIG PROBLEM
  |              ^^^^^^ mutable borrow occurs here
7 | 
8 |     println!("{}, {}, and {}", r1, r2, r3);
  |                                -- immutable borrow later used here
```

---
```rust
let mut s = String::from("hello");

{
    let r1 = &mut s;
} // r1 goes out of scope here, so we can make 
  // a new reference with no problems.

let r2 = &mut s;
```
---

 This is a huge constraint and you *will* struggle with it ! :angry:

**BUT** 

With this restriction, Rust can **prevent data races** at compile time. :angel:

> A data race happens when these three behaviors occur:
>
> - Two or more pointers access the same data at the same time.
> - At least one of the pointers is being used to write to the data.
> - There’s no mechanism being used to synchronize access to the data.


---
# Dangling reference ?

A dangling pointer is a pointer that leads to a freed variable.

```rust
fn main() {
    let reference_to_nothing = dangle();
}

fn dangle() -> &String {
    let s = String::from("hello");

    &s
}
```
> :warning: This code does not compile !

---
```
error[E0106]: missing lifetime specifier
 --> src/main.rs:5:16
  |
5 | fn dangle() -> &String {
  |                ^ expected named lifetime parameter
  |
  = help: this function's return type contains a borrowed value, but there is no value for it to be borrowed from
help: consider using the `'static` lifetime
  |
5 | fn dangle() -> &'static String {
  |                ^^^^^^^^
```

**Lifetime** is another concept unique to rust which will be covered later.

---
```cpp
#include <iostream>
#include <string>

class Object {};

Object *method() {
  Object object;
  return &object;
}

int main() {
    Object *o = method();
    return 0;
}
```
```
In function 'Object* method()':
8:10: warning: address of local variable 'object' returned
```
---
<!-- _header: '' -->
<!-- _paginate: false -->
# OOP-*ish*

---
<!-- header: 'OOP' -->
# `struct`s

```rust
struct User {
    username: String,
    email: String,
    sign_in_count: u64,
    active: bool,
}
```
```rust
let u = User {
    username: String::from("glatour"),
    email: String::from("guillaume.latour@irisa.fr"),
    active: true,
    sign_in_count: 2,
};
```

---
# Provide meaning to tuples


```rust
struct Coordinate(f64,f64);
struct Color(i32, i32, i32);
struct Point(i32, i32, i32);

// ofc you need explicitly typed variables
let rennes_pos: Coordinate = (48.1159675,-1.7234738);
let black: Color = (0,0,0);
let origin: Point = (0,0,0);
```

Enjoy rust type checking for your meaningful tuples!

---
# Methods
```rust
struct Rectangle {width: u32,height: u32}

fn main() {
    let rect1 = Rectangle {width: 30,height: 50};

    println!(
        "The area of the rectangle is {} square pixels.",
        area(&rect1)
    );
}

fn area(rectangle: &Rectangle) -> u32 {
    rectangle.width * rectangle.height
}
```

---
```rust
impl Rectangle {
    fn area(&self) -> u32 {
        self.width * self.height
    }
}
```
```rust
let rect1 = Rectangle {
    width: 30,
    height: 50,
};
```
```rust
rect1.area()
```
```rust
Rectangle::area(&rect1)
```

---

- The `Self` keyword can be use inside an `impl` block.
```rust
impl User {
    fn new(name: String) -> Self {
        Self{name}
    }
}
```

- Multiple `impl` blocks can be written, with the purpose of a better organization.

---
# `impl`ement existing `Trait`s

```rust
struct Rectangle {
    width: u32,
    height: u32,
}
fn main() {
    let rect1 = Rectangle {width: 30, height: 50};
    println!("rect1 is {}", rect1);
}
```
```
error[E0277]: `Rectangle` doesn't implement `std::fmt::Display`
```

---
![bg width:70%](images/fmt_display_desc.png)

---
![bg width:80%](images/fmt_display_exemple.png)

---
# implement the `Display` trait
```rust
use std::fmt;
impl fmt::Display for Rectangle {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {

        write!(f, "[{}*{}]", self.width, self.height)

    }
}
```

```
rect1 is [30*50]
```

---
<!-- _header: '' -->
<!-- _paginate: false -->
# `enum`s

*algebraic data type* from functional world

---
<!-- header: 'Enum' -->
# Defining an `enum`
```rust
enum IpAddrKind {
    V4,
    V6
}
```

```
let ipv4 = IpAddrKind::V4;
let ipv6 = IpAddrKind::V6;
```

```rust
fn route(ip_kind: IpAddrKind) {}
```

---
# Store data in the `enum`

```rust
enum IpAddrKind {
    V4(u8,u8,u8,u8),
    V6(String)
}
```

---
# `impl`ement methods to `enum`
```rust
impl IpAddrKind {
    fn my_function(&self) {
        // do something
    }
}
```

---
# :star: `Option` type

There is no `NULL`or `nullptr` type or value in Rust.

> Tony Hoare called his null reference feature his "billion-dollar mistake"

> [...]I couldn’t resist the temptation to put in a null reference, simply because it was so easy to implement. This has led to innumerable errors, vulnerabilities, and system crashes, which have probably caused a billion dollars of pain and damage in the last forty years.

---
But still, we need a way to represent values that **could** be nothing.

Here goes the `Option` enum.

```rust
enum Option<T> {
    Some(T),
    None
}
```

```rust
let some_string = Some(String::from("hello"));
let some_int = Some(5);

let absent_number: Option<u32> = None; // have to declare the generic type
```

---
```rust
let x: u8 = 10;
let y: Option<u8> = Some(10);

let z = x + y;
```
```
error[E0277]: cannot add `Option<u8>` to `u8`
 --> src/main.rs:5:15
  |
5 |     let z = x + y;
  |               ^ no implementation for `u8 + Option<u8>`
  |
  = help: the trait `Add<Option<u8>>` is not implemented for `u8`
```

---
# Responsibility and reward of `Option`
- you have to extract the value from the `Option` before using it (and manage the case where it might be `None`)
- you won't encounter `null` values
- if a value has not the type `Option` you can safely use that value

---
# Pattern `match`ing
```rust
let optional_integer = Some(3);

match optional_integer {
    Some(i) => println!("The value is {}", i),
    None => println!("There is no value")
};
```

> reminder : `match`es are exhaustive !
---
## Sugar for `enum`s

```rust
let x: Option<u32>;

// x receive a value

if let Some(i) = x {
    // here we can use `i` as the value (type T) of the Option<T>
}
```

```rust
match x {
    Some(i) => {
        // here we can use `i`
    },
    None => {}
}
```

---
## More sugar for `Option`
```rust
let x: Option<u32>;

// x receive a value

let x = x.map(|i| i*i);
```

```rust
let x = match x {
    Some(i) => Some(i*i),
    None => None
}
```

---
# Extract or panic

If Rust catches an error at runtime, we say it **panics**.
You can trigger a runtime error with the macro `panic!()`
`thread 'main' panicked at 'explicit panic'`

If you want you can `unwrap` the value from an `Option`, causing Rust to **panic** if the value is `None`.

```rust
let x: Option<u32> = Some(32);
let y: u32 = x.unwrap();
```

---
<!-- _header: '' -->
<!-- _paginate: false -->
# Error handling

---
<!-- header: 'Error handling' -->
In many cases, Rust **requires you to acknowledge the possibility of an error** and take some action before compilation.

This way of doing helps you to **discover errors and handle them** before you've deployed to production.

- *recoverable* errors : missing file, wrong user input, internet connexion lost, ...
- *unrecoverable* errors : bugs that make Rust panics, like trying to access to a location beyond the end of an array, ...

Without `Exception` !

---
# `Result` type
```rust
enum Result<T, E> {
    Ok(T),
    Err(E),
}
``` 
---

For example, the call of function `std::fs::File::open` could lead to an error
- if the file does not exist, or 
- if the disk cannot read correctly the file when prompted to, or
- ...

The return type of this function is `std::result::Result<File, std::io::Error>`

So the function call might 
- succeed $\rightarrow$ a `File` handler from which we can read
- fail, $\rightarrow$ an `std::io::Error` which contains more information about the failure
- 
---
```rust
use std::fs::File;

fn main() {
    let f = File::open("hello.txt");

    let f = match f {
        Ok(file) => file,
        Err(error) => panic!("Problem opening the file: {:?}", error),
    };
}
```
```rust
let f = File::open("hello.txt").unwrap();
let f = File::open("hello.txt").expect("Problem opening the file");
```

---
```rust
use std::fs::File;
use std::io::ErrorKind;

fn main() {
    let f = File::open("hello.txt");

    let f = match f {
        Ok(file) => file,
        Err(error) => match error.kind() {
            ErrorKind::NotFound => match File::create("hello.txt") {
                Ok(fc) => fc,
                Err(e) => panic!("Problem creating the file: {:?}", e),
            },
            other_error => {
                panic!("Problem opening the file: {:?}", other_error)
            }
        },
    };
}
```

---
```rust
use std::fs::File;
use std::io::ErrorKind;

fn main() {
    let f = File::open("hello.txt").unwrap_or_else(|error| {
        if let ErrorKind::NotFound = error.kind() {
            File::create("hello.txt").unwrap()
        } else {
            panic!("Problem opening the file: {:?}", error);
        }
    });
}
```

---
# :star: Propagating Errors
```rust
// use [...]

fn read_username_from_file() -> Result<String, io::Error> {
    let f = File::open("hello.txt");

    let mut f = match f {
        Ok(file) => file,
        Err(e) => return Err(e),
    };

    let mut s = String::new();

    match f.read_to_string(&mut s) {
        Ok(_) => Ok(s),
        Err(e) => Err(e),
    }
}
```

---
```rust
use std::fs::File;
use std::io;
use std::io::Read;

fn read_username_from_file() -> Result<String, io::Error> {
    let mut f = File::open("hello.txt")?;
    let mut s = String::new();
    f.read_to_string(&mut s)?;
    Ok(s)
}
```

---
It is different to use `?` and `unwrap()` or `expect()` in case of failure :

- `?` will make the function return a type `Err(E)`
- `unwrap()` or `expect()` will make Rust panic !

The `?` operator has another ad

---
## Isn't it beautiful`?`
```rust
fn read_username_from_file() -> Result<String, io::Error> {
    let mut s = String::new();

    File::open("hello.txt")?.read_to_string(&mut s)?;

    Ok(s)
}

// Rust also provide a way to read file content easily : `std::fs::read_to_string(file: &str) -> String`
```
---
# To `panic!()` or not to `panic!()`

- `panic!()`, `unwrap()` and `expect()` are easy tools to use when prototyping
- when refactoring time comes, they leave clear markers on where to improve your code robustness
- your tests should make Rust panic if they fail (it's the behavior of `assert()`)

- you may also call `unwrap()` when you know more than the compiler
    ```rust
    use std::net::IpAddr;
    let home: IpAddr = "127.0.0.1".parse().unwrap(); // hardcoded valid ip address
    ```

- when failure is expected, the use and management of `Result` is appropriate

---
<!-- _header: '' -->
<!-- _paginate: false -->
# Traits

---
<!-- header: 'Traits' -->
A `trait` is a concept for **sharing functionality** across different structs.

It's similar to **interfaces** in other programming languages.

Since there is **no inheritance in Rust**, this will be the way to share similar behavior across ~~class~~ structs

---
# Defining and implementing a `trait`
Imagine we want a common behavior across these different structs: `NewsArticle`, `Tweet`.
```rust
pub trait Summary {
    fn summarize(&self) -> String;
}
```

---
```rust
pub struct NewsArticle {
    pub headline: String,
    pub location: String,
    pub author: String,
    pub content: String,
}
pub struct Tweet {
    pub username: String,
    pub content: String,
    pub replies: u32,
    pub retweets: u32,
}
```

---
```rust
impl Summary for NewsArticle {
    fn summarize(&self) -> String {
        format!("{}, by {} ({})", self.headline, self.author, self.location)
    }
}

impl Summary for Tweet {
    fn summarize(&self) -> String {
        format!("{}: {}", self.username, self.content)
    }
}
```

---
# Usage
You can use the trait methods like the struct methods
```rust
let tweet = Tweet {
    username: String::from("glatour"),
    content: String::from("lorem ipsum dolor si amet"),
    replies: 0,
    retweets: 0,
};

println!("1 new tweet: {}", tweet.summarize());
```

---
# :star: Default implementation
```rust
pub trait Summary {
    fn summarize(&self) -> String {
        String::from("(Read more...)")
    }
}
```

---
```rust
struct BlogPost {
    username: String,
    headline: String,
    content: String,
    read_time: u32
}

impl Summary for BlogPost {}
```

---
# :star: Trait as parameters
```rust
pub fn notify(item: &impl Summary) {
    println!("Breaking news! {}", item.summarize());
}
```
```rust
pub fn notify<T: Summary>(item: &T) {
    println!("Breaking news! {}", item.summarize());
}
```
```rust
pub fn notify<T>(item: &T)
where
    T: Summary,
{
    println!("Breaking news! {}", item.summarize());
}
```

---
<!-- _header: '' -->
<!-- _paginate: false -->
# Generic types

---
<!-- header: 'Generic types' -->
Generics come in handy when a similar behavior is repeated
```rust
fn largest_i32(list: &[i32]) -> i32 {
    let mut largest = list[0];

    for &item in list {
        if item > largest {
            largest = item;
        }
    }

    largest
}
```

---
```rust
fn largest_char(list: &[char]) -> char {
    let mut largest = list[0];

    for &item in list {
        if item > largest {
            largest = item;
        }
    }

    largest
}
```

---
It would be nice to write something like this
```rust
fn largest<T>(list: &[T]) -> T {
    let mut largest = list[0];

    for &item in list {
        if item > largest {
            largest = item;
        }
    }

    largest
}
```
> :warning: This code does not compile!

---
```
error[E0369]: binary operation `>` cannot be applied to type `T`
 --> src/main.rs:5:17
  |
5 |         if item > largest {
  |            ---- ^ ------- T
  |            |
  |            T
  |
help: consider restricting type parameter `T`
  |
1 | fn largest<T: std::cmp::PartialOrd>(list: &[T]) -> T {
  |             ^^^^^^^^^^^^^^^^^^^^^^
```

---
Lets do this
```rust
use std::cmp::PartialOrd
fn largest<T: PartialOrd>(list: &[T]) -> T {
    let mut largest = list[0];

    for &item in list {
        if item > largest {
            largest = item;
        }
    }

    largest
}
```
> :warning: This code does not compile!
---
```
error[E0508]: cannot move out of type `[T]`, a non-copy slice
 --> src/main.rs:2:23
  |
2 |     let mut largest = list[0];
  |                       ^^^^^^^
  |                       |
  |                       cannot move out of here
  |                       move occurs because `list[_]` has type `T`, which does not implement the `Copy` trait
  |                       help: consider borrowing here: `&list[0]`

error[E0507]: cannot move out of a shared reference
 --> src/main.rs:4:18
  |
4 |     for &item in list {
  |         -----    ^^^^
  |         ||
  |         |data moved here
  |         |move occurs because `item` has type `T`, which does not implement the `Copy` trait
  |         help: consider removing the `&`: `item`

error: aborting due to 2 previous errors
```

---

Well lets just force `T` to implement the `Copy` trait
```rust
use std::cmp::PartialOrd
fn largest<T: PartialOrd + Copy>(list: &[T]) -> T {
    let mut largest = list[0];

    for &item in list {
        if item > largest {
            largest = item;
        }
    }

    largest
}
```

---
# Generics in structs
```rust
struct Point<T> {
    x: T,
    y: T,
}

impl<T> Point<T> {
    fn x(&self) -> &T {
        &self.x
    }
}

impl Point<f32> {
    fn distance_from_origin(&self) -> f32 {
        (self.x.powi(2) + self.y.powi(2)).sqrt()
    }
}
```

---
# Performance of Generics

- Your code doesn’t run any slower using generic types than it would with concrete types.

- Rust is performing *monomorphization* of the code that is using generics at compile time. 

    > *Monomorphization* is the process of turning generic code into specific code by filling in the concrete types that are used when compiled.

- The compiler looks at all the places where generic code is called and generates code for the concrete types the generic code is called with.

---
<!-- _header: '' -->
<!-- _paginate: false -->
# :star: Lifetimes

---
<!-- header: 'Lifetimes' -->

Each reference in Rust has an associated **lifetime**, which is the scope for which that reference is valid.

Most of the time, lifetimes are implicit and inferred by the compiler; like the types are often implicit and inferred.

But some situations require you to explicitly annotate lifetimes the same way you sometimes need to explicitly annotate types.

---
```rust
{
    let r;                // ---------+-- 'a
                          //          |
    {                     //          |
        let x = 5;        // -+-- 'b  |
        r = &x;           //  |       |
    }                     // -+       |
                          //          |
    println!("r: {}", r); //          |
}                         // ---------+
```
> :warning: This code does not compile!

---
```rust
{
    let x = 5;            // ----------+-- 'b
                          //           |
    let r = &x;           // --+-- 'a  |
                          //   |       |
    println!("r: {}", r); //   |       |
                          // --+       |
}                         // ----------+
```

---
```rust
fn longest(x: &str, y: &str) -> &str {
    if x.len() > y.len() {
        x
    } else {
        y
    }
}
```
> :warning: This code does not compile!

---
```
error[E0106]: missing lifetime specifier
 --> src/main.rs:9:33
  |
9 | fn longest(x: &str, y: &str) -> &str {
  |               ----     ----     ^ expected named lifetime parameter
  |
  = help: this function's return type contains a borrowed value, but the signature does not say whether it is borrowed from `x` or `y`
help: consider introducing a named lifetime parameter
  |
9 | fn longest<'a>(x: &'a str, y: &'a str) -> &'a str {
  |           ^^^^    ^^^^^^^     ^^^^^^^     ^^^
```

---
# Lifetime syntax
```rust
&i32        // a reference
&'a i32     // a reference with an explicit lifetime
&'a mut i32 // a mutable reference with an explicit lifetime
```

---
# Lifetime syntax (functions signature)
```rust
fn longest<'a>(x: &'a str, y: &'a str) -> &'a str {
    if x.len() > y.len() {
        x
    } else {
        y
    }
}
```
> The annotations go in the function signature, not in the function body.

---
Here we only return the `x` reference so we let know that the return type will have the same lifetime as the `x` variable.
```rust
fn longest<'a>(x: &'a str, y: &str) -> &'a str {
    x
}
```

---
```rust
fn longest<'a>(x: &str, y: &str) -> &'a str {
    let result = String::from("really long string");
    result.as_str()
}
```
> :warning: This code does not compile!

Here we try to give the return type a lifetime that is not linked to the input variables $\rightarrow$ "c'est no good"

---
# Lifetime in `struct`s
Of course a `struct` can hold references, but we need to tell Rust that the value cannot be `drop`ped before our struct goes out of scope.
```rust
struct ImportantExcerpt<'a> {
    part: &'a str,
}
```

---
# Lifetime Elision

> Elision: the omission of a sound or syllable when speaking

```rust
fn first_word<'a>(s: &'a str) -> &'a str {
```
```rust
fn first_word(s: &str) -> &str {
```

Important Rust history because it's possible that in the future more rules emerges that allow more implicit lifetimes.

When the compiler has a doubt, it will let you know

---
# `'static` lifetime

There is one special lifetime : `'static`, which mean that the reference can live for the entire duration of the program.

Any string literals (hardcoded strings) have the `'static` lifetime.

```rust
let s: &'static str = "I have a static lifetime";
```
The text of this string is stored in the program's binary, which is always available.

---
<!-- _header: '' -->
<!-- _paginate: false -->
# Collections

---
<!-- header: 'Collections' -->
# Vectors
It's like an array but it can grow or shrink in size.

```rust
let mut v: Vec<i32> = Vec::new(); // notice the `mut`, optional type annotation
v.push(1);
v.push(2);
v.push(3);

let tab: Vec<i32> = vec![1,2,3];
```

---
# Collection are iterable
```rust
let mut tab = vec![1,2,3,4,5]; // need `mut` to update it !

for n in &mut tab { // mutable reference of tab
    *n = 10 * n;    // dereference element to modify its value
}

for num in &tab {
    println!("{}", num);
}
```

---
<!-- _header: '' -->
<!-- _paginate: false -->
# :star: Functional features

---
<!-- header: 'Functional features' -->
# Closure : anonymous functions
```rust
let closure = |var| {
    println!("doing something..");
    var += 10
}
```
```rust
let closure = |var: u32| -> u32 {
    println!("doing something..");
    var += 10
}
```

---
# Sweet sugar
```rust
fn  add_one_v1   (x: u32) -> u32 { x + 1 }
let add_one_v2 = |x: u32| -> u32 { x + 1 };
let add_one_v3 = |x|             { x + 1 };
let add_one_v4 = |x|               x + 1  ;
```

---
# But you still have to be coherent
```rust
let example_closure = |x| x;

let s = example_closure(String::from("hello"));
let n = example_closure(5);
```
> :warning: This code does not compile!

---
# Capturing environment with closure
```rust
let x = 4;

let equal_to_x = |z| z == x;

let y = 4;

assert!(equal_to_x(y));
```

---
# Functional iterators
```rust
let v1 = vec![1, 2, 3];

let mut v1_iter = v1.iter();

assert_eq!(v1_iter.next(), Some(&1));
assert_eq!(v1_iter.next(), Some(&2));
assert_eq!(v1_iter.next(), Some(&3));
assert_eq!(v1_iter.next(), None);
```

---
# Consuming iterators
```rust
let tab = vec![1,2,3];
let total = tab.iter().sum();
```

---
# Chain iterators
```rust
let tab = vec![1,2,3];
let iterator = tab.iter();
let other_iter = iterator.map(|x| x + 1);
```

> :warning: Warning: iterators are lazy and do nothing unless consumed.

---
# `collect` into a collection

```rust
let tab = vec![1,2,3];
let double: Vec<_> = tab.iter().map(|x| 2 * x).collect(); // have to specify the type of the collection
                                                          // but the collected values type is inferred
```
---
# Other operators
## `zip`: glue two iterator together
```rust
let tab = vec![1,2,3,4,5];
let op: i32 = (1..=5)
    .zip(tab)
    .map(|(a,b)| a * b)
    .sum();
```
---
## `chunks`: group iterator values by `chunk_size`
> :warning: Operator on **slices** !
```rust
let tab = vec![1,2,3,4,5,6,7,8,9];
let op: Vec<_> = tab[..]
    .chunks(2)
    .map(|small| small.iter().sum())
    .collect();
```

---
# Other operators

- `filter`
- `fold`
- `filter_map`
- `step_by`
- `min`
- `max`
- ...

<!-- _header: '' -->
# And much more !

- smart pointers
- :star: fearless concurrency
- :star: macros
- :star: tests & documentation
- `unsafe` keyword
- advanced pattern matching

---
<!-- _header: '' -->
<!-- _paginate: false -->
<!-- backgroundImage: url('images/thats_all_folks.jpg') -->
