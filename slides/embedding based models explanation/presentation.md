---
marp: true
paginate: true
backgroundImage: url('images/hero-background.jpg')
footer : 'UNamur 🤍 INRIA - Guillaume Latour'
---

<!-- _paginate: false -->
# 📄 Explaining Embedding-based Models for Link Prediction in Knowledge Graphs
[Luis Galárraga - 2021]

<!-- 

---

# Disclaimer

Only one week of reading, mistakes may have crept in.

:warning: Be vigilant and conciliatory 
-->

---
# Objective

An agnostic framework to explain link prediction in a KG.

<!-- This framework allows a user defined scope through a context $C$ -->

---
<!-- header: "Definitions" -->
# Definitions

1. Knowledge Graph
2. Fact
3. Atom
4. (Horn) Rules
5. Predictions
6. Link predictor
7. Explanation metrics

---
# Knowledge Graph

![left](images/example_kg_1.png) ![right](images/example_kg_2.png)

> Fast, Exact, and Exhaustive Rule Mining in Large Knowledge Bases 
> Jonathan Lajus Thesis

---

# Knowledge Graph

$\mathcal{K} = (\mathcal{V}, \mathcal{E}, l_v, l_e)$

with 
- $\mathcal{K}$ the knowledge graph 
- $\mathcal{V}$ the vertices set (*entities*)
- $\mathcal{E}$ the edges set (*relations*)
- $l_v$ the vertices label function<!--  such as $l_v : \mathcal{V} \rightarrow \mathcal{L}$ -->
- $l_e$ the edges label function<!--  such as $l_e : \mathcal{E} \rightarrow \mathcal{P}$ -->

<!-- with $\mathcal{L}$ the label set and  $\mathcal{P} \subset \mathcal{L}$ -->

![bg right w:100%](images/example_kg_1.png)

---
# Facts / Statement

Facts can be written as $p(s, o)$ with subject $s$, predicate $p$, and object $o$ and fill the domain $\mathcal{K} \subset \mathcal{L} \times \mathcal{P} \times \mathcal{L}$.

---
# Facts / Statement

- marriedTo(Barack, Michelle)
- marriedTo(Michelle, Barack)
- hasChild(Michelle, Sasha)
- hasChild(Michelle, Malia)

![bg right w:100%](images/example_kg_2.png)

---
# Atom


An atom is a statement where the subject $s$ or the object $o$ can be a variable.

- *unbounded* atoms have 2 variables
- *bounded* atoms have 1 variable
- *grounded* atoms have no variable (= statements)

---
# Atom

- marriedTo(**x**, **y**)
    *(unbounded)*
- hasChild(Barack, **z**) 
    *(bounded)*
- hasChild(Barack, Michelle)
    *(grounded)*

![bg right w:100%](images/example_kg_2.png)


---
# Horn rules

Formula like $B \implies H$
where
- $B$ is the *body*, a conjunction of atoms $\land_{1 \le i \le n} A_i$
- $H$ is the *head* atom
<!-- 
> $parent(x, z) ∧ nationality(z, y) ⇒ nationality(x, y)$

The mining of those rules in a KG establishes the explainability of the predicted $H$. -->


---
# Horn rules
- marriedTo(**x**, **y**) $\implies$ marriedTo(**y**, **x**)
- marriedTo(**x**, *y*) $\land$ hasChild(*y*, **z**) $\implies$ hasChild(**x**, **z**)
  
$~$
![w:400](images/example_kg_1.png)

---
# Horn rules

A Horn rule is *safe* if all the variables used in the head appear in the body
- *safe* : marriedTo(**x**, *y*) $\land$ hasChild(*y*, **z**) $\implies$ hasChild(**x**, **z**)
- *not safe* : marriedTo(**x**, *y*) $\land$ hasChild(*y*, *z*) $\implies$ hasChild(**x**, *w*)

A Horn rule is *closed* if each variable occurs in at least 2 atoms
- *closed* : marriedTo(*x*, **y**) $\land$ hasChild(**y**, *z*) $\implies$ hasChild(*x*, *z*)
- *not closed* : marriedTo(*x*, **y**) $\land$ marriedTo(*w*, **z**) $\implies$ $\neg$ marriedTo(*x*, *w*)

There is also this kind of rules
- livesIn(**x**, `US`) $\implies$ speaks(**x**, `English`) 

---
# Prediction

A prediction is a substitution of a rule with entities from the KG.

For a rule $R : B \implies H$

We say that $R$ *predicts* a fact $A$ 
$R \land \mathcal{K} \vdash A$

If $A \in \mathcal{K}$, we say that $R$ *predicts* $A$ *correctly*
$R \land \mathcal{K} \models A$

---
# Predictions

## Rule
marriedTo(**x**, *y*) $\land$ hasChild(*y*, **z**) $\implies$ hasChild(**x**, **z**)

## Predictions

- hasChild(Barack, Sasha)
- hasChild(Barack, Malia)


![bg right w:100%](images/example_kg_2.png)

---

# Predictions

## Rule
marriedTo(**x**, **y**) $\implies$ hasChild(**x**, **y**)

## Predictions

- hasChild(Barack, Michelle)
- hasChild(Michelle, Barack)


> To perform a *rule mining* task, it's important to generate counter-examples ! ($\mathcal{K^-}$)

![bg right w:100%](images/example_kg_2.png)

---
# Prediction metrics

- Support : the number of time a rule provide a correct output
- Confidence : ratio of correct predictions (support $\div$ all predictions)

The following rule is often verified, but we have to measure how often it's not.

> marriedTo(**x**, *y*) $\land$ hasChild(*y*, **z**) $\implies$ hasChild(**x**, **z**)

---
# Potential set

The *potential set* $\Omega (\mathcal{K})$ of a KG $\mathcal{K}$ is the set of all the statements constructed from a combination of the *entities* and *relations*

---
# Potential set $\Omega (\mathcal{K})$

- marriedTo(Elvis, Priscilla)
- marriedTo(Elvis, Lisa)
- marriedTo(Priscilla, Elvis)
- marriedTo(Priscilla, Lisa)
- marriedTo(Lisa, Elvis)
- marriedTo(Lisa, Priscilla)
- hasChild(Elvis, Priscilla)
- hasChild(Elvis, Lisa)
- ...

![bg right w:100%](images/example_kg_1.png)

---
# Link predictor

A *link predictor* is a function $f : \Omega (\mathcal{K}) \rightarrow \mathbb{R}$ that assigns scores to the statements in the *potential set* of a KG.

In the specific case of *embedding based linked predictors*, this function can be seen as the following composition : $f = \hat{f} \circ h$
with
- $\hat{f} : \mathbb{R}^k \rightarrow \mathbb{R}$
- $h : \Omega(\mathcal{K}) \rightarrow \mathbb{R}^k$ (conversion from facts to $k$-dimensional vectors)

$\hat{f}$ is the black box that we want to explain
$h$ is the embedding function

---
# Explanation

An *explanation* for the latent-feature-based link predictor $\hat{f}$ is a function $g : \mathbb{R}^{k'} \rightarrow \mathbb{R}$ that approximates $\hat{f}$.

---
# Explanation metrics

The quality of the explanation is measured by its *fidelity*, the degree to which $g$ mimics the black box $\hat{f}$.

An explanation can be classified by its *scope*
- global
- local

Usually, an accurate global explanation is complex and one may prefer simpler (local) explanations.

---
![bg 100%](images/big_kg.png)

---
<!-- header: "Process" -->
# Process

From a KG $\mathcal{K}$ and a link predictor $f$, the idea is to provide an explanation of the black box in $f$ as a set of Horn Rules with associated weight.

$\mathcal{K} + f \rightarrow$ Luis' Algorithm $\rightarrow \{ R_i^w \}$

---
# Example 1

Let say the following statement was produced by a link predictor $f$:
- hasChild(Elvis, Barack)

and an explanation weighted by a certain confidence is given with the rule :
- marriedTo(**x**, *y*) $\land$ marriedTo(**w**, *z*) $\implies$ hasChild(**x**,**w**)

> Would you trust this new link prediction ?
> 
> Was the provided explanation useful for your trust decision-making ?

<!-- > :warning: This rule is not likely to be mined because it's not *closed* (each variable occurs in at least 2 atoms) -->

---
# Example 2

Let say the following statement was produced by a link predictor $f$:
- hasChild(Barack, Sasha)

and the following rule was extracted by the algorithm :
- marriedTo(**x**, *y*) $\land$ hasChild(*y*, **z**) $\implies$ hasChild(**x**, **z**)

> Would you trust this new link prediction ?
> 
> Was the provided explanation useful for your trust decision-making ?

---



<!-- header: "Algorithm" -->
# Algorithm

With 
- a link predictor $\hat{f} : \mathbb{R}^k \rightarrow \mathbb{R}$
- a KG $\mathcal{K} = \mathcal{K}^+ \cup \mathcal{K}^-$
- a context $C$ such as $C \cap \mathcal{K} = \emptyset$

1. Create the potential set based on the context
2. Predict a score with embedded methods (RESCAL, HolE, TransE, ...)
3. Mine rules (AMIE, ...)
4. Use logistic regression to label it as valid link prediction or not

---
<!-- ![bg center w:80%](images/algo.png) -->

<!-- --- -->

<!-- header: "Results" -->
# Results

Multiple metrics are used to evaluate the fidelity of the rules mined :
- AUC-ROC
- MRR
- coverage (ratio of facts for which an explanation is provided)


---
<!-- header : '' -->
# Thank you for your attention :smiley:

--- 
<!-- header : '' -->
<!-- _paginate: false -->
<!-- backgroundImage: url('images/thats_all_folks.jpg') -->
