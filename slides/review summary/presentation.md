---
marp: true
paginate: true
backgroundImage: url('images/hero-background.jpg')
footer : 'UNamur 🤍 INRIA - Guillaume Latour'
---

<!-- _paginate: false -->
# Review summary

---

# What are they saying ?

- Lack of evaluation, lack of comparison
    - How good are the chosen metric to measure fidelity ?
    - State of the art approaches
- How well do the mined rules serve as explanations ?
  - What is the definition of explanation ?
- How the initial context C is chosen in practice ?
- (~20) Minor questions, typo & quick fix

---

# Lack of evaluation, lack of comparison

I think this is the main critic that has been made to this paper.

Reviewers want some experiments, results & comparison that prove the value of the proposed framework.

Recurrent remarks concern
- missing state-of-the-art discussion (combined symbolic and embeddings based predictions)
- legitimacy of current experiments (is use of ranking pertinent? current experiments are illustrative but do not provide evaluation)

---

# How well do mined rules serve as explanations ?


- "Are the mined rules sufficient for an human to understand anything ?"
- "do the provided rules explain (better than the other rules) the decisions made by the link predictor ?"
- something else ?


In both case it seems that a comparison between other rule language is mandatory. Here is some of them :

- Horn
- Association
- Existential
- Mix Horn-association

---

# How well do mined rules serve as explanations ?

This question leverages another that has been pined by the reviewers : "What definition of explanation is used ?" 

Apparently some definition are already available in the wild and must be (at least) mentioned. A reviewer was particularly found of the **causality**.

---

 # How the context C is chosen in practice ?

 Since at least one reviewer, Danai and I thought of this question, it seems pertinent highlight this.

 Another reviewer is also asking how are the training and testing set created and used and I think this is linked to this question.

 ---

 # Minor questions and fixes

 The minor things cover remarks such as "do not use red on blue in figure" or "(section 5.1) How many times is called Algorithm 1 ?"

 Reviewers ask minor precision and/or give minor comment concerning the layout of the paper.

 ---

 # What should I do now ?

 