---
marp: true
paginate: true
backgroundImage: url('images/hero-background.jpg')
footer : 'UNamur 🤍 INRIA - Guillaume Latour'
---

 ![bg fit](images/rust_comic.png)
 

---

# :crab: Rust for academic software development :mortar_board:

---
# Credits

- **Max Orok** : Considering Rust for scientific software — RustFest Global 2020 https://www.youtube.com/watch?v=PEoPrMkg8W4
- **Adam Kelly** : Rust, For Science! - RustFest Rome 2018 https://www.youtube.com/watch?v=mlqxTfpF9Q4
- **Steve Klabnik** : The history of Rust https://dl.acm.org/doi/10.1145/2959689.2960081
- **Ryan Eberhardt** : Designing a New Rust Class at Stanford: Safety in Systems Programming - 2020 https://reberhardt.com/blog/2020/10/05/designing-a-new-class-at-stanford-safety-in-systems-programming.html
- **leftoversalad** : art  https://leftoversalad.com/c/015_programmingpeople/

---

# Plan 

1. What is rust ?
2. What is academic software development ?
3. Why are they good together ?

---

<!-- header : ':crab: What is Rust ?' -->
![bg right:30% fit](images/rust_logo.png)
# Rust 

> “Rust is a systems programming language focused on three goals: **safety**, **speed**, and **concurrency**.”
*Rust Documentation*


Rust is syntactically similar to C++, but can guarantee memory safety by using a **borrow checker** to validate references.

Rust achieves memory safety **without garbage collection**.

---

![bg right:30% fit](images/rusty.png)
# Rust 
Rust is a **multi-paradigm** programming language designed for **performance** and **safety**, especially safe **concurrency**.


- imperative procedural
- object-oriented
- pure functional

---

Rust is designed by **Mozilla Research**,
- 2006 : Graydon Hoare begins in his garage
- 2009 : Mozilla begins to sponsor the project
- 2010 : compiler compiled in OCaml
- 2011 : compiler self-compiled
- 2015 : first stable release

most loved language every year in stack overflow survey since 2016.

---

#  Memory safety

- no dangling pointers
- data race
- No NULL type (Tony Hoare billion-dollar mistake)

*via* ownership, borrowing and lifetime; without a garbage collector.

---

# Ownership & Borrowing
 all values have an **unique owner** variable

- multiple immutable references
- one mutable reference
- borrow-checker

---

# Lifetime

mechanism asserting (at compile time) that the variable is still alive

```rust
// This code does not compile !
{
    let r;                // ---------+-- 'a
                          //          |
    {                     //          |
        let x = 5;        // -+-- 'b  |
        r = &x;           //  |       |
    }                     // -+       |
                          //          |
    println!("r: {}", r); //          |
}                         // ---------+
```
---

```
   Compiling playground v0.1.0 (/mnt/MobyDisk/Documents/Info/rust/playground)
error[E0597]: `x` does not live long enough
 --> src/main.rs:6:13
  |
6 |         r = &x;
  |             ^^ borrowed value does not live long enough
7 |     }
  |     - `x` dropped here while still borrowed
8 |
9 |     println!("r: {}", r);
  |                       - borrow later used here

error: aborting due to previous error

For more information about this error, try `rustc --explain E0597`.
error: could not compile `playground`

To learn more, run the command again with --verbose.
```

---

# Other technical information

- compiled
- strongly typed
- package manager & tools (cargo)
- multi platform tools & target compilation
- Foreign Function Interface (FFI) to call and be called by C (libraries for other languages)

---

# Other personal appreciation

- insanely good documentation 
- amazing compiler
- welcoming community
- quality tools

<!-- 
- example in comment will get tested
- generic parameter can be implement some traits 
  - type checking at compile time $\neq$  C++ duck typing
- absolute matching
-->

---
<!-- header: ':mortar_board: Academic software development' -->

# Context for writing scientific software

- small team
- limited time & resources
- correctness needed
- performance wished
- documentation appreciated

---

Usual development process of a scientific software

![bg 90%](images/scientific_development_process.png)

---
<!-- header: ':mortar_board: :heart: :crab:' -->

# Why rust for academic software ?

- classes of bugs are eliminated
- performance equivalent to C
- existing documentation is amazing
- built in documentation & tests tools

---

# Rust tries its best to prevent silly mistakes

```rust
// This code does not compile !
let a: f32 = 12.34;

let b = match a {
  12.34 => "one",
  15 => 2
}
```

- cannot use float for pattern matching
- cannot have different return type
- matching must be absolute

---

```rust
let a: f32 = 12.34;

let b = match a {
  i if i < 15.0 => "one",
  j if j < 56.0 => "two",
  _ => "other"
}
```

---

# Easy testing

```rust
fn factorial(n: u32) -> u32 {
    if n <= 2 {
        n
    } else {
        n * factorial(n - 1)
    }
}

#[test]
fn test_factorial() {
    assert_eq!(factorial(2), 2);
    assert_eq!(factorial(1), 1);
    assert_eq!(factorial(12), 479001600);
}
```

```bash
$ cargo test
```

---

# Documentation examples are tests

```rust
/// Computes the factorial function `x -> x!`
///
/// # Examples
/// ```
/// # use library::factorial;
/// let x = library::factorial(3);
/// assert_eq!(x, 6);
/// ```
pub fn factorial(n: u32) -> u32 {
    if n <= 2 {
        n
    } else {
        n * factorial(n - 1)
    }
}
```

```bash
$ cargo test
```

---
![bg right:60% fit](images/rust_documentation.png)

```bash
rust doc --open
```

---

# Iteration

```rust
pub fn sum_of_even_square(input: &[i32]) -> i32 {
    input
        .iter()
        .map(|i| i * i)
        .filter(|i| i % 2 == 0)
        .sum()
}
```

# Paralelization
```rust
use rayon::prelude::*;
pub fn sum_of_even_square_par(input: &[i32]) -> i32 {
    input
        .par_iter()
        .map(|i| i * i)
        .filter(|i| i % 2 == 0)
        .sum()
}
```
---

<!-- header : ':mortar_board: :heart: :crab: ?'-->
# arguments against rust

- young language (python ~30, c++ ~40)
- learning curve
  - but easier to write *good* code than another "hard to learn" language
- missing libraries


---
<!-- header: ':mortar_board: :heart: :crab:'-->

> Software engineering best practices are built into the language and core tools

---

- Taking ML to production with Rust: a 25x speedup (2019) https://www.lpalmieri.com/posts/2019-12-01-taking-ml-to-production-with-rust-a-25x-speedup/
- Reddit : who is using rust for ML in production ? (2020) https://www.reddit.com/r/rust/comments/fvehyq/d_who_is_using_rust_for_machine_learning_in/
  - libraries to use tensorflow, python packages, ...
  - compile python libraries to C & use FFI bindings

---
<!-- header : '' -->
# Thank you for your attention :smiley:

--- 
<!-- _paginate: false -->
<!-- backgroundImage: url('images/thats_all_folks.jpg') -->
